#!/usr/bin/env python3

import discord
from discord.ext import commands

import logging
import pathlib

LOGFILE = pathlib.Path(__file__).parent / "debug.log"

logger = logging.getLogger("discord")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename=LOGFILE, encoding="utf-8", mode="w")
handler.setFormatter(logging.Formatter(
    "%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
logger.addHandler(handler)


intents = discord.Intents.default()
bot = commands.Bot("wrd!", intents=intents)

@commands.is_owner()
@bot.command(help='Re/load an extension.', hidden=True)
async def reload(ctx, extension):
    try:
        bot.reload_extension(extension)
        # green checkmark
        await ctx.message.add_reaction('\u2705')
    except commands.errors.ExtensionNotLoaded:
        await ctx.send(f"Failed to load extension {extension}.")

bot.load_extension("wordgator")

with open("token.txt", "r") as fp:
    token = fp.read()

bot.run(token)
