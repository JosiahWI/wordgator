import asyncio
import random

import discord
from discord.ext import commands

class Game:
    """
    Represents an in-progress game in a guild.
    """

    def __init__(self, guild, channel, join_game_message_id, timeout):
        self.guild = guild
        self.channel = channel
        self.join_game_message_id = join_game_message_id
        self.timeout = timeout
        self.players = []
        self._host = None
        self._word = None
        self._timer = None

    def add_player(self, member):
        """
        Add a player to the game.
        
        Will only work if the game is currently joining players.
        """
        if self.join_game_message_id is not None:
            self.players.append(member)
        else:
            raise GameAlreadyRunning()

    async def start_after(self, waittime):
        """
        Start the game after x minutes, if at least 2 players have joined.
        """
        await asyncio.sleep(waittime)
        if len(self.players) < 2:
            await self.channel.send("Not enough players to start.")
        else:
            await self.start()

    async def start(self):
        """
        Start a round of the game.
        """
        # no players will be allowed to join in the middle of the round
        self.join_game_message_id = None
        self._host = self.players.pop(0)
        self._word = random.choice([
            "guinea pig",
            "platitude",
            "acquisition",
            "rhombus",
            "insinuate"
            ])
        await self.announce()
        # the players have two minutes to guess.
        self._timer = asyncio.create_task(self.start_timer(self.timeout))

    async def guess(self, member, word):
        if member not in self.players:
            return
        if word.lower() == self._word.lower():
            self._timer.cancel()
            await self.channel.send(f"{member.display_name} guessed the word!")
            await self.cleanup()

    async def announce(self):
        msg = f"{self._host.display_name} is the host!\n"
        for player in self.players:
            msg += f"{player.display_name}\n"
        await self.channel.send(msg)
        await self._host.send(f"You must describe the word {self._word}!")

    async def start_timer(self, timeout):
        """
        Start the timer. If the timer isn't cancelled, it's a loss.
        """
        await asyncio.sleep(timeout)
        await self.channel.send("You ran out of time!")
        self.cleanup()
        self.players.append(self._host.id)

    async def cleanup(self):
        self._host = None
        self._word = None
        self._timer = None

class WordGator(commands.Cog):
    """
    Main bot cog responsible for running the word game.
    
    Game state is not persistent and is not planned to be.
    """

    def __init__(self, bot):
        self.bot = bot
        self._games = {}

    @commands.command(help='play the game')
    async def play(self, ctx, seconds_per_round: int = 60):
        """
        Play the game!
        """
        running_game = self._games.get(ctx.guild.id)
        if running_game is not None:
            await ctx.send(
                f"A game is already running in {running_game.channel.mention}!")
            return
        if seconds_per_round > 300:
            await ctx.send(
                f"Timeout must be no more than 5 minutes (300 seconds)")
            return
        # game is not running in this guild, so we can start a new one
        join_msg = await ctx.send("Hit the check mark to join the game!")
        await join_msg.add_reaction("\u2705") # green check mark
        game = Game(
            ctx.guild, ctx.channel, join_msg.id, seconds_per_round)
        self._games[ctx.guild.id] = game
        asyncio.create_task(game.start_after(10)) # give players 1 minute to join

    @commands.command(help='stop the game')
    async def stop(self, ctx):
        """
        Stop the game.
        """
        # we might want to announce a winner so we will need this
        running_game = self._games.get(ctx.guild.id)
        if running_game is None:
            await ctx.send(
                "You haven't started playing, and you're already done?")
            return
        # if there is a game running
        del self._games[ctx.guild.id]
        await ctx.message.add_reaction("\u2705") # green checkmark

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if payload.guild_id is None:
            return
        # if not in a guild, should do nothing. Unless there is a None key.
        game = self._games.get(payload.guild_id)
        if game is None:
            return
        if payload.message_id != game.join_game_message_id:
            return
        # we do not want the bot joining hehe
        if payload.user_id == self.bot.user.id:
            return

        # we know there is a join_game_message_id so there is no error.
        game.add_player(payload.member)

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.guild is None:
            return
        game = self._games.get(message.guild.id)
        if game is None:
            return
        if message.channel != game.channel:
            return
        # yeah, we don't want the bot to be guessing
        if message.author.id == self.bot.user.id:
            return

        await game.guess(message.author, message.content)

    @commands.command(help="say hi")
    async def hi(self, ctx):
        """
        Say hi to the bot. Testing command.
        """
        await ctx.send(f"Hi there {ctx.author.display_name}!")

class GameAlreadyRunning(Exception):
    pass

def setup(bot):
    bot.add_cog(WordGator(bot))
